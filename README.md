Study - Questions followed by Answers
## Instructions

Open index.html


Upload a JSON file.


```
[
    {"textFront":"What file type should I use?",
    "textBack":"JSON"},
    {"imageFront":"Can I use images?",
    "imageBack":"http://127.0.0.1:8080/images/yes.png"}
]

```

You probably need a server for the images, using file:// probably won't work.

const shuffleArray = arr => arr.sort(() => Math.random() - 0.5);

var cards = [];
var isFront = true;

const filesElement = document.getElementById("files"); 
const cardElement = document.getElementById("card");
const keepElement = document.getElementById("keep");
const removeElement = document.getElementById("remove");
const instructionElement = document.getElementById("instructions");
const studyform = document.getElementById("studyform");
studyform.reset();

function getImageCardHTML(src){
    return "<img src='"+src+"'/>";
} 

function show(element){
    if (element.classList.contains('hidden')) {
        element.classList.remove("hidden");
    }
}
function hide(element){
    console.log("hiding");
    if (!element.classList.contains('hidden')) {
        element.classList.add("hidden");
    }
}

function renderCardFront(cardToRender){
    if(cardToRender.imageFront){
        cardElement.innerHTML = getImageCardHTML(cardToRender.imageFront);
    } else {
        cardElement.innerText =cardToRender.textFront;
    }
    hide(keepElement);
    hide(removeElement);
}
function renderCardBack(cardToRender){
    if(cardToRender.imageBack){
        cardElement.innerHTML = getImageCardHTML(cardToRender.imageBack);
    } else {
        cardElement.innerText = cardToRender.textBack;
    }
    show(keepElement);
    show(removeElement);
}
function renderNoCards(){
    hide(keepElement);
    hide(removeElement);
    cardElement.innerText = "Cards finished."
    show(filesElement);

}
function render(){
    show(cardElement);
    if(cards.length > 0){
        if(isFront){
            renderCardFront(cards[0]);
        } else {
            renderCardBack(cards[0]);
        }
    } else {
        renderNoCards();
    }
}
function flip(){
    isFront = !isFront;
}


function cardClick(){
    console.log("card click");
    if(isFront){
        flip();
        render();
    } 
}

function keepClick(){
    if(!isFront){
        isFront = true;
        var firstCard = cards.shift();
        if(firstCard){
            cards.push(firstCard);
            render();
        }
    } 
}
function removeClick(){
    if(!isFront){
        isFront = true;
        cards.shift();
        if(cards.length < 1){
            studyform.reset();
        }
        render();
    } 
}

function upload(event){
    var file = event.target.files[0];
    var reader = new FileReader();
    reader.onload = function(event){
        var data = event.target.result;
        try{
            cards = shuffleArray(JSON.parse(data));
            hide(filesElement);
            hide(instructionElement);
            render();
        } catch(ex){
            alert("Could not parse");
        }
    }

    reader.readAsText(file);

}



